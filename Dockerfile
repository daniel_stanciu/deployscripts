FROM tomcat:9-jdk8-temurin-focal

ARG app_file

RUN [ ! "$(ls -A /usr/local/tomcat/webapps)" ] && mv /usr/local/tomcat/webapps.dist/* /usr/local/tomcat/webapps

ADD ${app_file} /usr/local/tomcat/webapps

ADD tomcat-users.xml /usr/local/tomcat/conf/
ADD context.xml /usr/local/tomcat/webapps/manager/META-INF/
